package main

import (
	"archive/zip"
	"io/ioutil"
	"log"
	"net/http"
)

const url = `https://minio.ayufan.eu/test-bucket/artifacts.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=PCA71CIJQIM4Z3RS5AFT%2F20170520%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20170520T120044Z&X-Amz-Expires=432000&X-Amz-SignedHeaders=host&X-Amz-Signature=0f38cedc583c89b322a989195dbb7d61245f3f4d07baa62dc64b564744aca5f5`

func main() {
	log.SetFlags(log.Lshortfile)

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("CALL:", resp.Request.URL, resp.Status, resp.ContentLength)

	rs := NewHttpReadSeeker(resp)
	defer rs.Close()

	zip, err := zip.NewReader(rs, resp.ContentLength)
	if err != nil {
		log.Fatalln(err)
	}

	for _, zipFile := range zip.File {
		if zipFile.Name == "node_modules/babel-plugin-transform-object-rest-spread/README.md" {
			log.Println("File found!")
			file, err := zipFile.Open()
			if err != nil {
				log.Fatalln(err)
			}

			_, err = ioutil.ReadAll(file)
			if err != nil {
				log.Fatalln(err)
			}
		}
	}

	log.Println("DONE!")
}
